<?php
/**
 *
 * @category        snippets
 * @package         socialBaker
 * @author          Ruud Eisinga, erpe
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        see info.php
 * @version         see info.php
 *
 *
 */
/* -------------------------------------------------------- */

// Must include code to stop this file being access directly
if(defined('LEPTON_PATH') == false) { die("Cannot access this file directly"); }

global $socialDefaults, $followDefaults;

$locale['EN'] = "en_US";
$locale['NL'] = "nl_NL";
$locale['DE'] = "de_DE";
$locale['FR'] = "fr_FR";
$locale['IT'] = "it_IT";

$socialDefaults['twitter'] = array (
		'id' 		=> 1,
		'name' 		=> 'Twitter',
		'share' 	=> 'https://twitter.com/intent/tweet?url={URL}&text={STATUS}%20-%20{DESCRIPTION}',
		'icon' 		=> 'x-twitter.png'
	);
$socialDefaults['facebook'] = array (
		'id' 		=> 2,
		'name' 		=> 'Facebook',
		'share' 	=> 'https://www.facebook.com/sharer/sharer.php?u={URL}',
		'icon' 		=> 'facebook.png'
	);
$socialDefaults['googleplus'] = array (
		'id' 		=> 3,
		'name' 		=> 'GooglePlus',
		'share' 	=> 'https://plus.google.com/share?url={URL}',
		'icon' 		=> 'googleplus.png'
	);
$socialDefaults['linkedin'] = array (
		'id' 		=> 4,
		'name' 		=> 'LinkedIn',
		'share' 	=> 'https://www.linkedin.com/shareArticle?mini=true&url={URL}&title={STATUS}&summary={DESCRIPTION}&source=',
		'icon' 		=> 'linkedin.png'
	);

$socialDefaults['digg'] = array (
		'id' 		=> 5,
		'name' 		=> 'Instagram',
		'share' 	=> 'http://www.digg.com/submit?url={URL}&title={STATUS}',
		'icon' 		=> 'digg.png'
	);
	
$socialDefaults['stumbleupon'] = array (
		'id' 		=> 6,
		'name' 		=> 'StumbleUpon',
		'share' 	=> 'http://www.stumbleupon.com/submit?url={URL}&title={STATUS}',
		'icon' 		=> 'stumbleupon.png'
	);
	
$socialDefaults['delicious'] = array (
		'id' 		=> 7,
		'name' 		=> 'Delicious',
		'share' 	=> 'https://delicious.com/save?v=5&noui&jump=close&url={URL}&title={STATUS}',
		'icon' 		=> 'delicious.png'
	);
$socialDefaults['vk'] = array (
		'id' 		=> 8,
		'name' 		=> 'VKontakte',
		'share' 	=> 'http://vk.com/share.php?url={URL}&title={STATUS}',
		'icon' 		=> 'vk.png'
	);
$socialDefaults['reddit'] = array (
		'id' 		=> 9,
		'name' 		=> 'Reddit',
		'share' 	=> 'http://www.reddit.com/submit?url={URL}&title={STATUS}',
		'icon' 		=> 'reddit.png'
	);
$socialDefaults['tumblr'] = array (
		'id' 		=> 10,
		'name' 		=> 'Tumblr',
		'share' 	=> 'http://www.tumblr.com/share?v=3&u={URL}&t={STATUS}',
		'icon' 		=> 'tumblr.png'
	);
$socialDefaults['email'] = array (
		'id' 		=> 11,
		'name' 		=> 'Email',
		'share' 	=> 'mailto:?subject=[STATUS]&body=[DESCRIPTION]%0D%0A%0D%0A{URL}',
		'icon' 		=> 'email.png'
	);

	


function socialBaker($status = null, $imagePath = null, $shares = null) {
	global $socialDefaults, $oLEPTON;
	$url = 'http'.($_SERVER['SERVER_PORT'] == 443 ? 's' : '') . '://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	$description = DESCRIPTION;
	$image = '';
	if(!$status) $status = page_status(); 			// if no status (message) is set use Pagetitle
	if(is_array($status)) {
		if(isset($status['url'])) $url = $status['url'];
		if(isset($status['image'])) $image = $status['image'];
		if(isset($status['description'])) $description = $status['description'];
		if(isset($status['status'])) $status = $status['status']; else $status = PAGE_TITLE;
	}
	if(!$imagePath) $imagePath = LEPTON_URL.'/modules/socialbaker/images/'; // if imagePath is not set, use internal icons
	
	if(!$shares) $shares = $socialDefaults; 	// if shares are not set, use all..
	if(!is_array($shares)) $shares = explode(",",$shares);
			
	$retval = '<div class="socialwrapper">';
	foreach ( $shares as $share ) {
		if(!is_array($share)) { 				//element is just a string or number.. find default values..
			if (is_numeric($share)) {
				foreach ($socialDefaults as $tmp) {
					if($tmp['id'] == $share) {
						$share = $tmp;
						break;
					}
				}
			} elseif(is_string($share)) { 			//element is string, just use it as index of the defaults array
				if(isset($socialDefaults[strtolower($share)])) {
					$share = $socialDefaults[strtolower($share)];
				}
			} 
		}
		if(is_array($share)) {
			$share['share'] = str_ireplace('{URL}',urlencode($url),$share['share']);
			$share['share'] = str_ireplace('{IMAGE}',urlencode($image),$share['share']);
			$share['share'] = str_ireplace('{DESCRIPTION}',urlencode($description),$share['share']);
			$share['share'] = str_ireplace('{STATUS}',urlencode($status),$share['share']);
			$share['share'] = str_ireplace('[DESCRIPTION]',$description,$share['share']);
			$share['share'] = str_ireplace('[STATUS]',$status,$share['share']);
			$class = substr($share['share'],0,7) == "mailto:" ? 'email' : 'share';
			$retval .= '<div class="socialbaker"><a class="'.$class.' '.strtolower($share['name']).'" title="'.$share['name'].'" href="'.$share['share'].'"><img class="shareicon" src="'.$imagePath.$share['icon'].'" alt="'.$share['name'].'" /></a></div>'.PHP_EOL;
		}
	}
	$retval .= '</div>';
	return $retval;
}



// FollowBaker

$followDefaults['twitter'] = array (
	'title' 	=> 'Follow us on Twitter',
	'link' 		=> 'https://twitter.com/{ACCOUNT}',
	'icon' 		=> 'twitter.png'
);
$followDefaults['facebook'] = array (
	'title' 	=> 'Follow us on Facebook',
	'link' 		=> 'https://www.facebook.com/{ACCOUNT}',
	'icon' 		=> 'facebook.png'
);
$followDefaults['googleplus'] = array (
	'title' 	=> 'Follow us on Google',
	'link' 		=> 'https://plus.google.com/{ACCOUNT}',
	'icon' 		=> 'googleplus.png'
);
$followDefaults['linkedin'] = array (
	'title' 	=> 'LinkedIn',
	'link' 		=> 'https://linkedin.com/in/{ACCOUNT}',
	'icon' 		=> 'linkedin.png'
);

$followDefaults['vk'] = array (
	'title' 	=> 'Follow us on VK',
	'link' 		=> 'https://www.vk.com/{ACCOUNT}',
	'icon' 		=> 'vk.png'
);


$followDefaults['youtube'] = array (
	'title' 	=> 'YouTube',
	'link' 		=> 'http://youtube.com/user/{ACCOUNT}',
	'icon' 		=> 'youtube.png'
);

$followDefaults['vimeo'] = array (
	'title' 	=> 'Vimeo',
	'link' 		=> 'http://vimeo.com/{ACCOUNT}',
	'icon' 		=> 'vimeo.png'
);

$followDefaults['instagram'] = array (
	'title' 	=> 'Instagram',
	'link' 		=> 'http://instagram.com/{ACCOUNT}',
	'icon' 		=> 'instagram.png'
);

$followDefaults['pinterest'] = array (
	'title' 	=> 'Pinterest',
	'link' 		=> 'http://pinterest.com/{ACCOUNT}',
	'icon' 		=> 'pinterest.png'
);

$followDefaults['flickr'] = array (
	'title' 	=> 'Flickr',
	'link' 		=> 'http://flickr.com/photos/{ACCOUNT}',
	'icon' 		=> 'flickr.png'
);


function followBaker($follows = null, $imagePath = null) {
	global $followDefaults;
	if(!$imagePath) $imagePath = LEPTON_URL.'/modules/socialbaker/images/'; // if imagePath is not set, use internal icons
	$retval = '<div class="followwrapper">';
	foreach ( $follows as $follow => $account ) {
		if($data = $followDefaults[$follow]) {
			$data['link'] = str_ireplace('{ACCOUNT}',$account , $data['link']);
			$retval .= '<div class="followbaker"><a target="_blank" class="follow '.strtolower($follow).'" title="'.$data['title'].'" href="'.$data['link'].'"><img class="followicon" src="'.$imagePath.$data['icon'].'" alt="'.$data['title'].'" /></a></div>'.PHP_EOL;
		}
	}
	$retval .= '</div>';
	return $retval;
}

function shareBaker($shares = null) {
	return socialBaker(null,null,$shares);
}


function openGraph($use_short = false, $show_title = false) {
	if(!PAGE_ID) return '';
	$settings = _get_page_settings(); // get page/news/bakery settings

	$retval = '';

	if($settings['homepage']) {
		$settings['link'] = '';
		$settings['page_link'] = LEPTON_URL;
	}
	
	if(!$settings['title']) $settings['title'] = PAGE_TITLE;
	if($show_title) $retval .= '<title>'.WEBSITE_TITLE.' - '.$settings['title'].'</title>'."\n\t";

	//canonical link
	if($use_short) {
		$retval .= "".'<link rel="canonical" href="'.LEPTON_URL.$settings['link'].'/" />';
	} else {
		$retval .= "".'<link rel="canonical" href="'.$settings['page_link'].'" />';
	}
	
	//facebook and google+ stuff
	$retval .= "\n\t".'<meta property="og:title" content="'.$settings['title'].'" />';
	$retval .= "\n\t".'<meta property="og:description" content="'.$settings['description'].'" />';
	$retval .= "\n\t".'<meta property="og:site_name" content="'.$settings['site_name'].'" />';
	$retval .= "\n\t".'<meta property="og:type" content="'.$settings['type'].'" />';
	$retval .= "\n\t".'<meta property="og:locale" content="'.$settings['locale'].'" />';
	if($use_short) {
		$retval .= "\n\t".'<meta property="og:url" content="'.LEPTON_URL.$settings['link'].'/" />';
	} else {
		$retval .= "\n\t".'<meta property="og:url" content="'.$settings['page_link'].'" />';
	}
	if($image = _get_page_image($settings)) {
		$retval .= "\n\t".'<meta property="og:image" content="'.$image['url'].'" />';
		$retval .= "\n\t".'<meta property="og:image:width" content="'.$image['width'].'" />';
		$retval .= "\n\t".'<meta property="og:image:height" content="'.$image['height'].'" />';
	}
	//twitter stuff
	$cardtype = ($image && $image['width'] > 250) ? "summary_large_image":"summary";
	$retval .= "\n\t".'<meta name="twitter:card" content="'.$cardtype.'" />';
	$retval .= "\n\t".'<meta name="twitter:site" content="'.$settings['site_name'].'"/>';
	$retval .= "\n\t".'<meta name="twitter:title" content="'.$settings['title'].'"/>';
	$retval .= "\n\t".'<meta name="twitter:description" content="'.$settings['description'].'"/>';
	if($use_short) {
		$retval .= "\n\t".'<meta name="twitter:url" content="'.LEPTON_URL.$settings['link'].'"/>';
	} else {
		$retval .= "\n\t".'<meta name="twitter:url" content="'.$settings['page_link'].'" />';
	}
	if($image) {
		$retval .= "\n\t".'<meta name="twitter:image" content="'.$image['url'].'"/>';
		$retval .= "\n\t".'<meta name="twitter:image:width" content="'.$image['width'].'" />';
		$retval .= "\n\t".'<meta name="twitter:image:height" content="'.$image['height'].'" />';
	}
	$retval .= "\n";
	return $retval;
}


function jsonld($use_short = false) {
	global $oLEPTON, $database;
	$bread_crumbs = $oLEPTON->page_trail;
	$count = sizeof($bread_crumbs);
	$r = '';
	$counter = 2;
	
	$bread_crumb_items = '{"@type": "ListItem","position": 1,"item": {"@id": "'.LEPTON_URL.'","name": "'.WEBSITE_TITLE.'"}},';
	foreach ($bread_crumbs as $temp) {
		$sql  = 'SELECT * FROM `'.TABLE_PREFIX.'pages` WHERE `page_id`='.(int)$temp;
		$query_menu = $database->query($sql);
		$page = $query_menu->fetchRow();
		$url = page_link($page['link']);
		$bread_crumb_items .= '{"@type": "ListItem", "position": '.($counter) . ',"item": {"@id": "' . $url .'","name": "'.$page['menu_title'].'"}},';
		//$r .= "\n".'<a data-count="'.$counter.'" href="'.page_link($page['link']).'" class="link">'.$page['menu_title'].'</a>';
		$counter++;
	}	
	$bread_crumb_items = rtrim($bread_crumb_items,',');
	$bread_crumb = '"@type": "BreadcrumbList","itemListElement": ['.$bread_crumb_items.']}';
	$webpage = '<script type="application/ld+json">{ "@context": "http://schema.org",' .$bread_crumb. '}</script>'."\n";
	return $webpage;
}



function page_status() {
	global $database,$post_id,$item_id;
	$title = PAGE_TITLE;
	if(isset($post_id )) {
		$title = $database->get_one("SELECT `title` from ".TABLE_PREFIX."mod_news_posts WHERE `post_id` = '$post_id'");
	} elseif (isset($item_id) || defined('ITEM_ID') ) {
		if($db = getDbForItemId()) {
			if(!isset($item_id))$item_id = ITEM_ID;
			$title = $database->get_one("SELECT `title` from ".TABLE_PREFIX."mod_".$db."_items WHERE `item_id` = '$item_id'");	
		}
	}
	return $title;
}

function _get_page_settings() {
	global $oLEPTON, $database, $locale, $post_id, $item_id, $topic_id;
	$page = null;
	$db = '';
	if(isset($post_id)) {
		$page = $database->query("SELECT *, link as menu_title, content_short as description FROM " . TABLE_PREFIX . "mod_news_posts WHERE `post_id`='".$post_id."'");
	} elseif(isset($topic_id) && file_exists(LEPTON_PATH.'/modules/topics/module_settings.php')) {
		require(LEPTON_PATH.'/modules/topics/module_settings.php');
		$page = $database->query("SELECT *, link as menu_title FROM " . TABLE_PREFIX . "mod_topics WHERE `topic_id`='".$topic_id."'");
	} elseif(isset($item_id)) {
		if($db = getDbForItemId()) {
			$page = $database->query("SELECT *, link as menu_title FROM " . TABLE_PREFIX."mod_".$db."_items WHERE `item_id`='".$item_id."'");
		} else {
			$page = $database->query("SELECT *, page_title as title FROM ".TABLE_PREFIX."pages WHERE `page_id` = '".PAGE_ID."'");
		}
	} 
	if(!$page || $page->numRows()==0) {
		$page = $database->query("SELECT *, page_title as title FROM ".TABLE_PREFIX."pages WHERE `page_id` = '".PAGE_ID."'");
	}
	$settings = $page->fetchRow();
	$settings['homepage'] = $oLEPTON->default_page_id == PAGE_ID;
	$settings['description'] = trim(strip_tags(@$settings['description']));
	$settings['site_name'] = WEBSITE_TITLE;
	$settings['type'] = 'website';
	if(!isset($settings['language'])) $settings['language'] = LANGUAGE;
	$settings['locale'] = isset($locale[$settings['language']])?$locale[$settings['language']]:strtolower($settings['language']).'_'.$settings['language'];
	$settings['page_link'] = $oLEPTON->page_link($settings['link']);
	if(isset($item_id)) {
		if($db == 'bakery') {
			//$pdir = $database->get_one("SELECT `pages_directory` FROM " . TABLE_PREFIX."mod_bakery_general_settings LIMIT 1");
			//$settings['link'] = '/'.$pdir.$settings['link'];
			$settings['page_link'] = LEPTON_URL.PAGES_DIRECTORY.$settings['link'].PAGE_EXTENSION;
		} else {
			$settings['link'] = $oLEPTON->page['link'].$settings['link'];
			$settings['page_link'] = LEPTON_URL.PAGES_DIRECTORY.$oLEPTON->page['link'].$settings['link'].PAGE_EXTENSION;
		}
	}
	if(isset($topic_id)) {
		$settings['page_link'] = htmlspecialchars(LEPTON_URL.$topics_directory.$settings['link'].PAGE_EXTENSION);
		$settings['description'] = trim(strip_tags($settings['content_short']));
	}
	return $settings;
}
function _get_page_image($settings = array()) {
	global $database, $topic_id, $post_id,$item_id;
	$menu_title = $settings['menu_title'];
	$retval = array();
	if(isset($item_id)) {
		if($db = getDbForItemId()) {
			if($db == 'bakery') { // Bakery
				if(file_exists(LEPTON_PATH.'/modules/bakery/config.php')) { // Baker 1.7+
					require LEPTON_PATH.'/modules/bakery/config.php';
					$image = $database->get_one("SELECT `filename` FROM " . TABLE_PREFIX."mod_bakery_images WHERE `item_id`='".$item_id."' ORDER BY `position`");
					$retval['filename'] = LEPTON_PATH.MEDIA_DIRECTORY.'/'.$img_dir.'/images/item'.$item_id.'/'.$image;
					$retval['url'] = LEPTON_URL.MEDIA_DIRECTORY.'/'.$img_dir.'/images/item'.$item_id.'/'.$image;
				} else {
					$image = $database->get_one("SELECT `main_image` FROM " . TABLE_PREFIX."mod_bakery_items WHERE `item_id`='".$item_id."'");
					$retval['filename'] = LEPTON_PATH.MEDIA_DIRECTORY.'/bakery/images/item'.$item_id.'/'.$image;
					$retval['url'] = LEPTON_URL.MEDIA_DIRECTORY.'/bakery/images/item'.$item_id.'/'.$image;
				}
			} else {
				$image = $database->get_one("SELECT `filename` FROM " . TABLE_PREFIX."mod_".$db."_images WHERE `item_id`='".$item_id."' ORDER BY `position`");
				$retval['filename'] = LEPTON_PATH.MEDIA_DIRECTORY.'/'.$db.'/images/item'.$item_id.'/'.$image;
				$retval['url'] = LEPTON_URL.MEDIA_DIRECTORY.'/'.$db.'/images/item'.$item_id.'/'.$image;
			}
		}
	} elseif(isset($topic_id)) {
		$picture_dir = $database->get_one("SELECT `picture_dir` FROM ".TABLE_PREFIX."mod_topics_settings WHERE page_id = '".PAGE_ID."'");
		$retval['filename'] = LEPTON_PATH.$picture_dir.'/'.$settings['picture'];
		$retval['url'] = LEPTON_URL.$picture_dir.'/'.$settings['picture'];
	} elseif(isset($post_id) && file_exists(LEPTON_PATH.MEDIA_DIRECTORY.'/posts/'.$post_id.'.jpg')) {
		$retval['filename'] = LEPTON_PATH.MEDIA_DIRECTORY.'/posts/'.$post_id.'.jpg';
		$retval['url'] = LEPTON_URL.MEDIA_DIRECTORY.'/posts/'.$post_id.'.jpg';
	} elseif(file_exists(LEPTON_PATH.MEDIA_DIRECTORY.'/pages/'.PAGE_ID.'.jpg')) {
		$retval['filename'] = LEPTON_PATH.MEDIA_DIRECTORY.'/pages/'.PAGE_ID.'.jpg';
		$retval['url'] = LEPTON_URL.MEDIA_DIRECTORY.'/pages/'.PAGE_ID.'.jpg';
	} elseif(file_exists(LEPTON_PATH.MEDIA_DIRECTORY.'/pages/'.$menu_title.'.jpg')) {
		$retval['filename'] = LEPTON_PATH.MEDIA_DIRECTORY.'/pages/'.$menu_title.'.jpg';
		$retval['url'] = LEPTON_URL.MEDIA_DIRECTORY.'/pages/'.$menu_title.'.jpg';
	} 
	if(!count($retval)) {
		if(file_exists(LEPTON_PATH.MEDIA_DIRECTORY.'/pages/website.jpg')) {
			$retval['filename'] = LEPTON_PATH.MEDIA_DIRECTORY.'/pages/website.jpg';
			$retval['url'] = LEPTON_URL.MEDIA_DIRECTORY.'/pages/website.jpg';
		} 
	}
	if(!count($retval)) return false;
	if(!file_exists($retval['filename'])) {
		$retval = array();
		return $retval;
	}
	list($width, $height) = getimagesize($retval['filename']);
	$retval['width'] = $width;
	$retval['height'] = $height;
	return $retval;
}

function getDbForItemId () {
	global $database;
	if(!defined('ITEM_ID')) return '';
	$item_id = ITEM_ID;
	$page_id = PAGE_ID;
	$modules = $database->query("SELECT `module` FROM ".TABLE_PREFIX."sections WHERE `page_id`='$page_id'");
	if($modules && $modules->numRows()) {
		while ($module = $modules->fetchRow()) {
			$m = $module['module'];
			$res = $database->query("SHOW TABLES LIKE '" . TABLE_PREFIX . "mod_".$m."_items'");
			if($res && $res->numRows()) {
				return $m;
			}
		}
	}
}


?>
