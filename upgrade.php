<?php
/**
 *
 * @category        snippets
 * @package         socialBaker
 * @author          Ruud Eisinga, erpe
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        see info.php
 * @version         see info.php
 *
 *
 */
/* -------------------------------------------------------- */

// Must include code to stop this file being accessed directly
if(!defined('LEPTON_PATH')) {
		die('Access denied!!');
}

LEPTON_handle::delete_obsolete_files('/modules/socialbaker/images/twitter.png');