<?php
/**
 *
 * @category        snippets
 * @package         socialBaker
 * @author          Ruud Eisinga, erpe
 * @license         http://www.gnu.org/licenses/gpl.html
 * @platform        see info.php
 * @version         see info.php
 *
 *
 */
/* -------------------------------------------------------- */

header('Location: ../index.php');
