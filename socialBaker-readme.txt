After installing the snippet simply add - on the position you want te buttons to appear - the following code:

<?php echo socialBaker(); ?>

This call will add a block with all configured icons.
The snippet has a number of parameters to modify the output.

The parameters are:

1) status
With this parameter it is possible to change the default data that will be posted to the social media. The data used is depending on the social media itself.
By default the page title, the current url and the page description is used.
To change this, an array with these parameters must be constructed and added as the first parameter.

Example:
	
<?php
$status = array (
    'status' => 'Beautifull article',
    'url' => 'http://myserver.com/the_article',
    'description' => 'A nice article about beatifull websites'
);
echo socialBaker($status);
?>

2) imagePath
Using this parameter is is possible to change the directory from where the icons will be loaded.
If you have your own set of images located in /media/social/ you can give this path as a second parameter.
If you do not want to change the status parameter you can use the value null or '' instead.

Example
	
<?php
echo socialBaker(null,'/media/social/');
?>

3) shares
With this parameter it is possible to tell the snippet what shares (and what order) you want to be displayed.
You can do this as a simple number or by adding the name of a service in a comma seperated list.
If you do not want to use the first two parameters you can use the value null or '' instead.

Example:
	
<?php
echo socialBaker(null,null,'1,2,3,4'); // just use twitter/facebook/google-plus/linkedin
?>

or
	
<?php
echo socialBaker(null,null,'twitter,facebook,googleplus,linkedin');
?>

The numbers or servicenames to use are:

1 = twitter
2 = facebook
3 = googleplus
4 = linkedin
5 = digg
6 = stumbleupon
7 = delicious
8 = vk
9 = reddit
10 = tumblr