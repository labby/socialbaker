After installing the snippet simply add - on the position you want te sharing buttons to appear - the following code:
	
<?php echo shareBaker(); ?>

This call will add a block with all configured icons.
The snippet has one parameter to modify the output.

The parameters are:

1) shares
With this parameter it is possible to tell the snippet what shares (and what order) you want to be displayed.
You can do this as a simple number or by adding the name of a service in a comma seperated list.
If you do not want to use the first two parameters you can use the value null or '' instead.

Example:
	
<?php
echo shareBaker('1,2,3,4'); // just use twitter/facebook/google-plus/linkedin
?>

or
	
<?php
echo shareBaker('twitter,facebook,googleplus,linkedin');
?>

The numbers or servicenames to use are:

1 = twitter
2 = facebook
3 = googleplus
4 = linkedin
5 = digg
6 = stumbleupon
7 = delicious
8 = vk
9 = reddit
10 = tumblr