After installing the snippet simply add - on the position you want te buttons to appear - the following code:

<?php echo followBaker($follows, $imagepath); ?>

This call will add a block with all services that will have an account configured.

The parameters are:

1) follows
This parameter holds all accounts that you want to have displayed as a follow button.
It is an array with a key > value pair per service.

Example:
	
<?php

$follows['twitter'] = 'mytwitter';
$follows['facebook'] = 'myfacebook';
$follows['googleplus'] = '+mygoogleplus';
$follows['linkedin'] = 'mylinkedin';
$follows['youtube'] = 'myyoutube';
$follows['vimeo'] = 'myvimeo';
$follows['instagram'] = 'myinstagram';
$follows['pinterest'] = 'mypinterest';
$follows['flickr'] = 'myflickr';
$follows['vk'] = 'myvkontakte';
echo followBaker($follows);
?>

2) imagePath
Using this parameter is is possible to change the directory from where the icons will be loaded.
If you have your own set of images located in /media/social/ you can give this path as a second parameter.
If you do not want to change the status parameter you can use the value null or '' instead.

Example
	
<?php
echo socialBaker($follows,'/media/social/');
?>
