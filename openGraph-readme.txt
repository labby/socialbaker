After installing the snippet simply add the following code in the <head> </head> section of your website:

<?php echo openGraph(); ?>

This call will add a block with all tags needed by Facebook and Google plus
The snippet has one optional parameter (true or false) to modify the output when shorturl is used.

The following call will generate the link without /pages/ and .php (short_url method).
<?php echo openGraph(true); ?>

Images...
=========
An important feature of the openGraph protocol is telling the calling website what image to use.
For this you need to create a directory /pages/ in the media directory.
The snippet will scan that location for images in the following way.

1) For Topics, Bakery and OneForAll pages the linked image will be used.
2) else scan for /media/pages/{PAGE_ID}.jpg
	for newsitems the scan is for /media/posts/{POST_ID}.jpg
3) else scan for /media/pages/{MENU_TITLE}.jpg (case sensitive!)
4) else scan for /media/pages/website.jpg (default image for the website)

If using these rules no images are found, there will not be an "og:image" output.

News and Bakery
===============
The snippet will detect a news post or a bakery item and use their details to publish.
New: Now also oneforall pages are detected (from version 2.4). For these pages (and for Bakery) the main image will be used.